package com.example.facerecognition

import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.acuant.acuantcamera.camera.AcuantCameraActivity
import com.acuant.acuantcamera.constant.*
import com.acuant.acuantcommon.exception.AcuantException
import com.acuant.acuantcommon.initializer.AcuantInitializer
import com.acuant.acuantcommon.initializer.IAcuantPackageCallback
import com.acuant.acuantcommon.model.Credential
import com.acuant.acuantfacecapture.FaceCaptureActivity
import com.acuant.acuantfacecapture.model.FaceCaptureOptions
import com.acuant.acuantimagepreparation.initializer.ImageProcessorInitializer
import com.example.facerecognition.databinding.ActivityMainBinding
import java.io.*

class MainActivity : AppCompatActivity() {
   private val REQUEST_CODE = 22
    lateinit var binding :ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initializeImageProcessor()
    }

    private fun initializeImageProcessor() {
        try{
            try{
                val callback = object:IAcuantPackageCallback{
                    override fun onInitializeSuccess() {
                        startCameraActivity()
                    }

                    override fun onInitializeFailed(error: List<com.acuant.acuantcommon.model.Error>) {

                    }
                }
                //I initialized here
                //because the xml file with these settings did not work
                 Credential.init("Kevin.Fox@w2globaldata.com",
                    "d2GEyWr<wm^1EL+S",
                    "2949dd6f-e747-4ecf-a038-cade9cb7bfae ",
                    "https://frm.acuant.net",
                    "https://services.assureid.net",
                    "https://medicscan.acuant.net",
                   "https://us.passlive.acuant.net",
                   "https://acas.acuant.net",
                   "https://ozone.acuant.net")
                AcuantInitializer.initialize("", this, listOf(ImageProcessorInitializer()), callback)
            }
            catch(e: AcuantException){
                Log.e("Acuant Error", e.toString())
            }
        }
        catch(e: AcuantException){
            Log.e("Accunt Erro", e.toString())
        }

    }

    private fun startCameraActivity() {
        val cameraIntent = Intent(
            this@MainActivity,
            FaceCaptureActivity::class.java
        )


        cameraIntent.putExtra(ACUANT_EXTRA_FACE_CAPTURE_OPTIONS, FaceCaptureOptions())

        startActivityForResult(cameraIntent, REQUEST_CODE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            when (resultCode) {
                FaceCaptureActivity.RESPONSE_SUCCESS_CODE -> {
                    val bytes = readFromFile(data?.getStringExtra(FaceCaptureActivity.OUTPUT_URL))
                    val capturedSelfieImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                    binding.imageCaptured.setImageBitmap(capturedSelfieImage)
                }
                FaceCaptureActivity.RESPONSE_CANCEL_CODE -> {
                    //handle user canceling
                }
                else -> {
                    //handle error during capture
                }
            }
        }
    }


    /**
     * method from the sdk
     */

    private fun readFromFile(fileUri: String?): ByteArray{
        val file = File(fileUri)
        val bytes = ByteArray(file.length().toInt())
        try {
            val buf = BufferedInputStream(FileInputStream(file))
            buf.read(bytes, 0, bytes.size)
            buf.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception){
            e.printStackTrace()
        }
        return bytes
    }

}